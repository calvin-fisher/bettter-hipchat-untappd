# Untappd #

This HipChat add-on uses Untappd.com data and Foursquare location data to figure out what beers are on tap at the location that you give it!

![HipChat - Search Results.png](https://bitbucket.org/repo/boE6j4/images/2414136269-HipChat%20-%20Search%20Results.png)

Syntax

```
#!html

/tap at [name of pub/bar] near [location]
```


### [Install Me](https://hipchat.com/addons/install?url=https%3A%2F%2Fac-koa-hipchat-untappd.herokuapp.com%2Faddon%2Fcapabilities) ###

