var untappd = require('./untappd');

module.exports = function (notifier) {
  return {

    at: function *(args) {
      var match = /\s*(.+?)\s+(?:near|in)\s+(.+?)\s*$/.exec(args);
      if (!match) {
        return yield notifier.send('Sorry, I didn\'t understand that.');
      }
      var venue = yield untappd.findVenue(match[2], match[1]);
      if (venue) {
        var address = venue.location.address + ', ' + venue.location.city + ', ' + venue.location.state;
        yield notifier.sendTemplate('beers', {
          url: 'https://untappd.com/venue/' + venue.venue_id,
          venue: venue,
          address: address,
          beers: venue.top_beers.items
        });
      } else {
        yield notifier.send('Sorry, I didn\'t find a matching venue.');
      }
    }

  };
};
